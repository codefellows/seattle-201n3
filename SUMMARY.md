# Summary

**1**: Introduction to Web Design

* [Class 01](1-web-dev-intro/class-01)
* [Class 02](1-web-dev-intro/class-02)
* [Class 03](1-web-dev-intro/class-03)
* [Class 04](1-web-dev-intro/class-04)
* [Class 05](1-web-dev-intro/class-05)
* [Class 06](1-web-dev-intro/class-06)

**2**: Object Oriented Programming and the DOM

* [Class 07](2-oop-dom/class-07)
* [Class 08](2-oop-dom/class-08)
* [Class 09](2-oop-dom/class-09)
* [Class 10](2-oop-dom/class-10)
* [Class 11](2-oop-dom/class-11)
* [Class 12](2-oop-dom/class-12)

**3**: Interactive Web Design

* [Class 13](3-interactive-web/class-13)
* [Class 14](3-interactive-web/class-14)
* [Class 15](3-interactive-web/class-15)
* [Class 16](3-interactive-web/class-16)
* [Class 17](3-interactive-web/class-17)
* [Class 18](3-interactive-web/class-18)

**4**: Project Week and Presentation:

* [Milestone 4](4-final-projects/class-19-project-week-day-1)
* [Milestone 5](4-final-projects/class-20-project-week-day-2)
* [Milestone 1](4-final-projects/class-21-project-week-day-3)
* [Milestone 2](4-final-projects/class-22-project-week-day-4)
* [Milestone 3](4-final-projects/class-23-project-week-day-5)
* [Milestone 3](4-final-projects/class-24-project-week-day-6)
