# Class 6

# Reading Reminder
* ["Color" (HTML & CSS, Ch. 11)](https://canvas.instructure.com/courses/1015286/modules/items/9246694)
* ["Images" (HTML & CSS, Ch. 5)](https://canvas.instructure.com/courses/1015286/modules/items/9246693)

# Assignment for Class
In this assignment you will be further extending your guessing game by including images and working with colors. Once complete you'll be working to deploy your code to the interwebs!

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246695)

## User Stories (MVP)
 - As a developer, I want to apply D.R.Y. to my code, so that I do not have duplicate and unnecessary code.
 - As a developer, I want to include images in my game, when the user guesses correctly, so that the game is more informative and fun.
 - As a developer, I want to color code the answers on-screen, so my user can visually see what answers were right or wrong.
 - As a developer, I want to host my game online so that anyone can play!!

## Technical Requirements
 - Proper use of arrays and functions when applying DRY
 - Images are correctly sized and styled
 - Color properly applied on right/wrong answers
 - GH-Pages deployment fully functional

## Helpful Resources
- [**Supporting Information**](support.md)

## Lecture Vids
[Part 1](https://youtu.be/lVOUi1nBoA8)
[Part 2](https://youtu.be/w22VzaYmefQ)
[Part 3](https://youtu.be/X9i_tkM4mWk)
