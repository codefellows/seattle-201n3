# seattle-201n3
### Introduction to Web Development

###Class 01
1. Intro to Code Fellows
2. Introductions

+++BREAK+++

3. Intro to Dev Env (Tool Belt & Browser basics)
4. Intro to User Stories
5. HTML & JS Basics
6. GitHub Gists

###Class 02
1. HTML Basics
2. JavaScript Basics
3. Debugging Basics

+++BREAK+++

4. Git & GitHub
5. README.md / Documentation

###Class 03
1. Code Review
2. Control Flow

+++BREAK+++

4. Logical Operators & Counters
5. Github Branches & PR's
6. Separation of Concerns

###Class 04
1. Code Review & Survey Comments
2. Arrays
3. Functions

+++BREAK+++

3. HTML Links
4. Basic DOM Manipulation
5. Pair Programming Basics

###Class 05
1. Code Review
2. HTML5 / Semantics vs Style
3. HTML Lists

+++BREAK+++

4. Introduction to CSS
5. Pair Programming

###Class 06
1. Code Review
3. Color
4. Images

+++BREAK+++

5. Git Review
6. Deployment
8. (if we have time) What languages / courses interest you?
