# Class 5
[**Week 2 Feedback**](https://canvas.instructure.com/courses/1015286/modules/items/9246692)
*You will receive an email from Code Fellows with a link to the survey. Once complete, please complete this assignment as well.*

# Reading Reminder
* ["Introducing CSS" (HTML & CSS, Ch. 10)](https://canvas.instructure.com/courses/1015286/modules/items/9246685)
* ["Lists" (HTML & CSS, Ch. 3)](https://canvas.instructure.com/courses/1015286/modules/items/9246689)

# Assignment for Class
Today will be your second exercise in pair programming. You will be assigned to work with a partner, and the two of you will work on and extend each other's guessing games.

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246690)

## User Stories (MVP)
 - As a developer, I want to further apply D.R.Y. to my code, so that I do not have duplicate and unnecessary code.
 - As a developer, I want to add some basic styling to my site, so that I can increase the happiness and engagement of my users.
 - As a member of a team, I want to list a link to my partner's repository on my page, so the world knows that we worked in harmony together

## Technical Requirements
 - Proper syntax and refactoring when applying D.R.Y.
 - CSS stylesheet linked to html
 - Basic CSS properties and values used, such as color, font-size, and font-family

## User Stories (Stretch)
 - As a developer, I want my site to look more professional, so my users will enjoy their experience (hint: Add more specific CSS styling to make more pretty)

## Helpful Resources
- [**Supporting Information**](support.md)
