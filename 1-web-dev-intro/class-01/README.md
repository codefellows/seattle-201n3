# Class 1

# Reading Reminder

* ["Structure" (HTML & CSS, Ch. 1)](https://canvas.instructure.com/courses/1015286/modules/items/9246671)
* ["Extra Markup" (HTML & CSS, Ch. 8)](https://canvas.instructure.com/courses/1015286/modules/items/9246672)
* ["Process & Design" (HTML & CSS, Ch. 18)](https://canvas.instructure.com/courses/1015286/modules/items/9246673)
* ["The ABC of Programming" (JavaScript, Ch.1 )](https://canvas.instructure.com/courses/1015286/modules/items/9246670)

# Assignment for Class
In class, we wrote a program that accepted user input and, based on that input, displayed a message back to the user. Your assignment in lab today involves a variety of different activities related to that.

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246674)

## User Stories (MVP)
 - As a developer, I want to create a simple bio / resume page, so that my users can get to know me.
 - As a developer, I want to ask the user for their name and alert them with a personalized Hello, so they know I care!

## Technical Requirements
 - Work will be done in an HTML file in Atom, and then submitted via Canvas (copy and paste into submission)
 - Good use of HTML; e.g. 'body', 'doctype', 'p', 'head', 'html', 'h1', and 'title'
 - HTML is cleanly written and indented for readability and organization
 - JavaScript written to alert the user in a "Hello, World" type of script

## Helpful Resources
- [**Supporting Information**](support.md)

## Lecture Vids
[Part 1](https://youtu.be/lBkSN42-aT0)
[Part 2](https://youtu.be/Wgu70S2q4sk)
[Part 3](https://youtu.be/qUa1URipAnE)
[Part 4](https://youtu.be/5OINogbE7PE)
