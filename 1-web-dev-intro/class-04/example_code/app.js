// Don't forget that we wrapped this whole file in an IIFE! So, arr does not exist in the global scope.
(function() {
  // This is oldschool for creating an array
  var arr = new Array(); // arr => [];
  arr = new Array(4); // arr => [undefined, undefined, undefined, undefine];

  // This is normal for creating an array;
  arr = []; // arr => [];
  arr = [1, 4, 'Blue', true, 'False']; // arr => [1, 4, 'Blue', true, 'False']
  arr = [[1, 4], ['Blue', true, ['False']]];
  // How do we get at 'False'?
  arr[1][2][0]; // => 'False'    Not to be confused with ['False'] from arr[1][2]

  // Modifying the array
  arr = [1, 2, 3, 4, 5];
  arr[4] = 99; // Reassign the value 5 to 99. arr => [1, 2, 3, 4, 99]
  arr[4] = 5; // Reassign the value 99 back to 5. arr => [1, 2, 3, 4, 5]

  // Common array methods to be away of
  arr.indexOf(1); // Will return 0, showing that the value of 1 is at the 0 index
  arr.indexOf(44); // Will return -1, showing that the value does not exist in the array
  arr.push(6); // Will add value to the end of the array. arr => [1, 2, 3, 4, 5, 6]
  var popVal = arr.pop(); // Will remove AND return value from the end of the array. arr => [1, 2, 3, 4, 5]; popVal => 6;
  arr.unshift(0); // Will add value to the beginning of the array. arr => [0, 1, 2, 3, 4, 5]
  var shiftVal = arr.shift(); // Will remove AND return value from the beginning of the array. arr => [1, 2, 3, 4, 5]; shiftVal => 0;
  var spliceArr = arr.splice(1, 3); // Will remove AND return section of array from starting to ending index as arguments;
      // arr => [1, 5];
      // spliceArr => [2, 3, 4];
})();
