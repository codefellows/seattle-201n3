(function() {
  var correctAnswers = 0;
  console.log(correctAnswers);
  var userName = prompt('Tell me your name.');

  document.getElementById('intro').textContent = 'Welcome to the game ' + userName;

  var els = [
    document.getElementById('answerOne'),
    document.getElementById('answerTwo'),
    document.getElementById('answerThree'),
    document.getElementById('answerFour')
  ];

  var questions = [
    'What\'s my middle name?',
    'What city do I live in now?',
    'Do I have two dogs?',
    'Guess how old you think I am.'
  ];

  var answers = [
    'jared',
    'maple valley',
    'yes',
    33
  ];

  function game(question, answer, element) {
    var counter = 4;
    var userInput = prompt(question).toLowerCase();

    if (isNaN(parseInt(userInput)) === false) {
      userInput = parseInt(userInput);
    }

    if (typeof answer === 'number') {
      while (userInput !== answer) {
        if (counter === 0) {
          break;
        }

        if (isNaN(userInput)) {
          userInput = parseInt(prompt('Try guessing a number.\n You have ' + counter + ' guesses left.'));
          counter--;
        } else if (userInput > answer) {
          userInput = parseInt(prompt('Too high, guess again.\n You have ' + counter + ' guesses left.'));
          counter--;
        } else {
          userInput = parseInt(prompt('Too low, guess again.\n You have ' + counter + ' guesses left.'));
          counter--;
        }
      }

      if (counter > 0) {
        correctAnswers++;
        element.textContent = "Congrats! That's the right answer: " + answer;
      } else {
        element.textContent = "Sorry, " + answer + " was the wrong number";
      }
    } else {
      if (userInput === answer) {
        correctAnswers++;
        element.textContent = "Congrats! That's the right answer: " + answer.charAt(0).toUpperCase() + answer.slice(1);
      } else {
        element.textContent = "Sorry, " + answer.charAt(0).toUpperCase() + answer.slice(1) + " was the right answer";
      }
    }
    document.getElementById('info').textContent = 'You currently have answered ' + correctAnswers + ' question(s) correct.';
  }

  for (var i = 0; i < questions.length; i++) {
    game(questions[i], answers[i], els[i]);
  }
})()
