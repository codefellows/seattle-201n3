# Class 4

# Reading Reminder
* [“Functions, Methods & Objects” (JavaScript, Ch. 3)](https://canvas.instructure.com/courses/1015286/modules/items/9246684)
* ["Links" (HTML & CSS, Ch. 4)](https://canvas.instructure.com/courses/1015286/modules/items/9246688)

# Assignment for Class
Today will be your first exercise in **pair programming**. You will be assigned to work with a partner, and the two of you will work on and extend each other's guessing games.

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246686)

## User Stories (MVP)
 - As a developer, I want my code written in a modular way, so that I can reuse functionality without duplicating code.
 - As a developer, I want to work with a partner, so that we can write code more efficiently and effectively.
 - As a developer, I want to link to my partner's repository from my web page, so I can cite our efforts toward improving each other's code.
 - As a developer, I want my user to see the answers and responses on the web page, so we do not have to constantly interact with alerts.

## Technical Requirements
 - Updated and cleanly documented README, including partner's name and link to their repository
 - Game functionality is encapsulated in functions
 - Good use of external links to partner's repository
 - Alerts have been removed from the game, and replaced by printing answers to the DOM.
 - Partners have forked, completed work, and then submitted a pull request back to the source repo

## User Stories (Stretch)
- As a developer, I want to add a sixth question to my game, which has multiple possible answers (hint: using an array and for loop)
- As a developer, I want to add some basic styling to my site, so that I can increase the happiness and engagement of my users (hint: do not use inline styling for this; you should link to another file for your styling)

## Helpful Resources
- [**Supporting Information**](support.md)

## Lecture Vids
[Part 1](https://youtu.be/80V4B3w5HFw)
[Part 2](https://youtu.be/NPMWi-WJHwA)
[Part 3](https://youtu.be/Uepfe3bHgic)


## Pairs for pair programming
1. Steven | Jonathan
2. Patrick | Nurbek
3. Thomas | J.R.
5. Jessica | Geoffrey
6. Jeff | Dee
7. Cody | Chris
