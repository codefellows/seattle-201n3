var userName = prompt('Tell me your name.');
var message = 'My cat has brown stripes.';
console.log(message);
if (userName === 'Scott') {
  message = 'That\'s right, Scott was the right answer';
} else if (userName === 'Bambi') {
  message = 'That\'s right, Bambi was the right answer';
} else {
  message = 'You were way off!';
}
console.log(message);

var arr = [1, 3, 'Blue', true, 'Dog'];
for (var i = arr.length - 1; i >= 0; i--) { // for (start, stop, step) {}
  console.log(arr[i] + ': ' + typeof arr[i]);
  if (typeof arr[i] === 'string') { // Console.log arr[i] if it's a number data type
    console.log(arr[i]);
  }
}

do {
  var userNum = parseInt(prompt('Guess my number'));
} while (userNum !== 10) {
  console.log(userNum);
  userNum = parseInt(prompt('Guess again.'));
}

// This is giving the user too many guesses - Can you figure out why?
// We want to give the user 3 guesses.
var counter = 0;
var userNum = parseInt(prompt('Guess my number'));
while(counter <= 2) {
  if (userNum === 10) {
    alert('You nailed it.');
    break;
  } else if (userNum > 10) {
    alert('Your number was too high');
    counter++;
    console.log('The counter is at ' + counter);
    // counter += 1;
    // counter = counter + 1;
    userNum = parseInt(prompt('Guess lower.'));
  } else if (userNum < 10) {
    alert('Your number was too low');
    counter++;
    console.log('The counter is at ' + counter);
    userNum = parseInt(prompt('Guess higher.'));
  }
}
