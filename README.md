# Code 201: Foundations of Software Development
Welcome to sea-201n3!

**Instructors & TA's**

*Instructor*: Scott Schmidt

*TA's*
- Thomas Phillips
- Kate Lucas

# Introduction
Build a strong software development foundation and learn how to use HTML, CSS, JavaScript, and various libraries to create fully-functional web apps.

Through studying computer science concepts, such as data structures and algorithms, and common development tools, including Atom, Git, and Terminal, you will learn the methods and tools of professional software developers.

The general course schedule is:

**Part Time**    | Topic
-------------|---------------
Part 1 | Introduction to Web Development
Part 2 | Object Oriented Programming and the DOM
Part 3 | Interactive Web Design
Part 4 | Project Weeks!

Check out the full [Table of Contents](SUMMARY.md).

## Logistics
**Location**:
  Code Fellows  
  511 Boren Ave North, Seattle 98109

**Room**: In "The East/Hardy” classroom on the Basement level

**Instructional Lecture: The Easy**
- Monday: 6:30pm to 9:30pm;
- Saturday: 9am to 12pm (lunch 12pm-1pm).
**Instructional Lab: The Hardy**
- Wednesday: 6:30pm to 9:30pm;
- Saturday: 1pm-4pm
**Co-Work Lab: The Hardy**
- Tuesday & Thursday: 6:30pm to 930pm

**Homework**: There are reading and coding assignments several times a week.

**Duration**:
Night courses meet Monday through Thursday, plus one weekend day. In weeks two through six, 2 weeknight classes will be unattended co-working lab time.
* First class: April 5, 2016
* Last class: May 28, 2016

## Lecture Notes from class

Lecture Notes will be placed in each class' lecture-code subdirectory after they are presented.  
Lecture Notes will be a markdown file in the class repository.   

## Assignments

Lab-time code assignments follow each class time. You can find these assignments in the subdirectories of each class folder. Assignment content will very from day to day, depending on the topic. Some of the lab assignments will be pair programming assignments.

Instructions for completing and submitting the assignments can be found in the README files of the appropriate subdirectories.

Assignments are submitted using a professional-grade git-flow. If you haven't done so already, it's time to get comfortable with the core git commands. Don't worry, we'll give you step by step help as needed.

# Required Computer Setup
* Atom
* Git
* OSX, Linux, or Windows with Git-Bash installed.
* Chrome Browser

## Install Atom

If you haven't already, install [Atom](https://atom.io). If you have used an advanced text editor like Sublime Text, then Atom will feel familiar to you. Atom is free, open-source, cross-platform, and has a wide array of useful plug-ins available. Please use Atom during Code 201. (And yes...if you are proficient with another text editor that you *love*, you may use that instead)

[Atom's documentation](https://atom.io/docs/latest) is top-notch. Review it now to familiarize yourself with the basics. Make sure you're looking at the docs for the latest version.

## Install linter and linter-eslint Atom packages

For this next part, you will have Atom's package manager **apm** install some packages. Go [here](https://atom.io/docs/v0.194.0/using-atom-atom-packages#command-line) to verify it's enabled. Once you have verified that apm commands will work, enter this on your Terminal:

`apm install linter linter-eslint`

You should get two success messages while it installs the linter and linter-eslint packages.

### Verify the Atom packages installation
Enter the following into your Terminal:

`apm ls`

![](http://i.imgur.com/Jlv6LeP.png)

You should get a long list and at the end you should get a list of packages you installed for Atom. Linter and linter-eslint should be on that list.

Congrats! You're all done.
