# Class 10

# Reading Reminder
* ["Events" (JavaScript, Ch. 6)](https://canvas.instructure.com/courses/1015286/modules/items/9246710)
* ["Forms" (HTML & CSS, Ch. 7)](https://canvas.instructure.com/courses/1015286/modules/items/9246709)
* ["Tables" (HTML & CSS, Ch. 6)](https://canvas.instructure.com/courses/1015286/modules/items/9246708)

# Assignment for Class
Today in lab you will be doing some work to add an input form to your sales data page. You will also be working with your sales data page to wire up the form and use it to update your table data.

We will also be replacing the lists of data for each store, and implementing a table of data instead!

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246711)

## User Stories (MVP)
 - As a developer, I want to implement and form on the website, so Pat has the ability to enter new stores without a developer to help him
 - As a developer, I want to implement event management on my page, so that my form will listen and react to click events
 - As a developer, I want to present the store data in a table format on the webpage, so each store's data is easier to represent and understand

## Technical Requirements
 - Form properly implemented in the html
 - Events have been wired up and are rendering new store data to the page
 - Table has been implemented and lists are no longer used to represent store data

## User Stories (Stretch)
 - As a user, I want to be able to update data that is already available on the page, so that I do not have to have the developer manually remove and rewrite data.

## Helpful Resources
[Problem Domain Document](../support.md)

[Assignment Assets](../assets)

## Lecture Vids
[Part 1](https://youtu.be/ZEAtpEIzx24)
[Part 2](https://youtu.be/pa2lvHyxMkk)
[Part 3](https://youtu.be/DFWEOvWCmp4)
