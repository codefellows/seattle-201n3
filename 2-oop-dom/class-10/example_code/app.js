// var submit = document.getElementById('test');

// function foo() {
//   // console.log('Hello World');
//   console.log(this);
//   // this.removeEventListener('click', foo);
// };

// submit.addEventListener('click', foo);

var formEl = document.getElementById('myForm');
var inputFN = document.getElementById('firstName');
var inputLN = document.getElementById('lastName');
var inputAge = document.getElementById('age');
var tableEl = document.getElementById('myTable');

// function getInputs(event) {
//   event.preventDefault();
//   console.log(event);
// }


// This is how we define elements and use document.getElementById to get values
// function hanler(event) {
//   event.preventDefault();
//   var trEl = document.createElement('tr');
//   var tdOne = document.createElement('td');
//   var tdTwo = document.createElement('td');
//   var tdThree = document.createElement('td');
//   tdOne.textContent = inputFN.value;
//   tdTwo.textContent = inputLN.value;
//   trEl.id = inputFN.value + inputLN.value;
//   tdThree.textContent = inputAge.value;
//   trEl.appendChild(tdOne);
//   trEl.appendChild(tdTwo);
//   trEl.appendChild(tdThree);
//   tableEl.appendChild(trEl);
// }

formEl.addEventListener('submit', function(event) {
  event.preventDefault();
  console.log(event.target.firstName.value);
  var trEl = document.createElement('tr');
  var tdOne = document.createElement('td');
  var tdTwo = document.createElement('td');
  var tdThree = document.createElement('td');
  tdOne.textContent = event.target.firstName.value;
  tdTwo.textContent = event.target.lastName.value;
  trEl.id = event.target.firstName.value + event.target.lastName.value;
  tdThree.textContent = event.target.age.value;
  trEl.appendChild(tdOne);
  trEl.appendChild(tdTwo);
  trEl.appendChild(tdThree);
  tableEl.appendChild(trEl);
});
