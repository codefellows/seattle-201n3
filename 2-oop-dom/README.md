# seattle-201n3
### Object Oriented Programming & the DOM

### Class 07
1. Reminders:
  1. arrays & `for (key in array) {}` syntax
  2. New Reop's & `.gitignore` file
  3. Class Scheduling - How is Sat / Mon Lecture?
2. Code Review
3. Domain Modeling

+++BREAK+++

4. Object Literals
5. DOM Rendering (lists)
6. New repo Review

###Class 08
1. Survey Responses
1. Code Review & JS WAT!

+++BREAK+++

2. Style guides
3. Document Object Model & DOM Methods

###Class 09
1. 301 Entrance Exam - Acceptance process
1. Code Review
1. ES-Lint - Linting!!!!
2. Text Formatting (CSS)

+++BREAK+++

3. Constructor Functions

###Class 10
1. Code Review & Survey Responses
2. Events
3. Forms

+++BREAK+++

4. HTML Tables
5. DOM Rendering (table)

###Class 11
1. Review - What have we learned so far?
1. GitHub Issues: Submit issues for lab help or other general assistance!
1. Code Review
2. Semantic HTML5

+++BREAK+++

3. Box Model

###Class 12
1. Code Review
2. CSS Layout

+++BREAK+++

3. Grid Systems
  - 960 Grid System (fixed)
  - **Skeleton** (fluid)
4. Animations (If we have time)
