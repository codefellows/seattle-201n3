# Class 9
[**Week 4 Feedback**](https://canvas.instructure.com/courses/1015286/modules/items/9246717)
*You will receive an email from Code Fellows with a link to the survey. Once complete, please complete this assignment as well.*

# Reading Reminder
* [“Text” (HTML & CSS, Ch. 12) ](https://canvas.instructure.com/courses/1015286/modules/items/9246705)

# Assignment for Class
In class, we learned how to refactor our cookie stand objects using a constructor and instances. Let's replace your object literals with a single constructor that when called with 'new', creates new instances. See pages 106-109 in your textbook for an example... and especially focus on 108 and 109.

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246706)

## User Stories (MVP)
 - As a developer, I want to implement a constructor function, so that I can reuse code and eliminate much of the duplication in my JavaScript
 - As a developer, I want to continue to evolve my styleguide (keep working on adding more details or finishing up what's not complete)

## Technical Requirements
 - Working on a non-master branch for the day, with regular commit history
 - Good use of a constructor function; style and syntax are correctly implemented
 - Duplicate code has been removed and DRY principles are evident
 - Styleguide is improving and becoming more specific (separate file from main page)

## User Stories (Stretch)
 - As a user, I want to be able to add a new store to my webpage, so that I have the ability to work independently of my developer
  - This is a large stretch goal. There will be Forms and Events involved, which you are welcome to read ahead on and try to implement.

## Helpful Resources
[Problem Domain Document](../assets/support.md)

[Assignment Assets](../assets)
