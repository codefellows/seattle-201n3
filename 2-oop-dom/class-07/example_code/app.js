var sectionEl = document.getElementById('ninja-turtles');

var turtles = [
  {
    name: 'Donatello',
    age: 19,
    ninja: true,
    food: 'Pizza',
    color: 'Purple',
    hunger: 4,
    eat: function(hungerLevel) {
      // This is a method because it is a function that belongs to an object.
      if (hungerLevel < 5) {
        console.log("I'm not that hungry, but I'll eat pizza, Brah");
        this.hunger -= 2;
      } else {
        console.log("I'm starvin' Marvin! Cowabunga!!");
        this.hunger -= 4;
      }
    },
    fight: function() {
      console.log(this.name);
      this.hunger += 2;
    }
  },
  {
    name: 'Leonardo',
    age: 19,
    ninja: true,
    food: 'Pizza',
    color: 'Blue',
    hunger: 8,
    eat: function(hungerLevel) {
      // This is a method because it is a function that belongs to an object.
      if (hungerLevel < 5) {
        console.log("I'm not that hungry, but I'll eat pizza, Brah");
        this.hunger -= 2;
      } else {
        console.log('I\'m starvin\' Marvin! Cowabunga!!');
        this.hunger -= 4;
      }
    },
    fight: function() {
      console.log(this.name);
      this.hunger += 2;
      this.weapon = 'Katana';
    }
  }
];

function game(q, a, ele) {
  // do some stuff
  // This is not a method, because it does not belong to an object.
}

var raphael = {
  name: 'Raphael',
  age: 19,
  ninja: true,
  food: 'Pizza',
  color: 'Red',
  hunger: 6,
  eat: function(hungerLevel) {
    // This is a method because it is a function that belongs to an object.
    if (hungerLevel < 5) {
      console.log("I'm not that hungry, but I'll eat pizza, Brah");
      this.hunger -= 2;
    } else {
      console.log("I'm starvin' Marvin! Cowabunga!!");
      this.hunger -= 4;
    }
  },
  fight: function() {
    console.log(this.name);
    this.hunger += 2;
    this.weapon = 'Sai';
  },
  render: function() {
    var ulEl = document.createElement('ul');
    var keys = Object.keys(this);
    for (var i = 0; i < 6; i++) {
      var liEl = document.createElement('li');
      liEl.textContent = this[keys[i]];
      ulEl.appendChild(liEl);
    }
    sectionEl.appendChild(ulEl);
  }
}


// BELOW EXAMPLES THAT WE DID NOT COVER IN CLASS - SUPPLIMENTAL NOTES AND EXAMPLES
// Not the prefered way to create an Object
// var turtle = Object();
// console.log(turtle);

// Empty object literal
var banana = {};
console.log(banana);

// Basic object literal with properties and one method (method commented out as an example only)
var turtle = {
  color: 'Blue',
  legs: 4,
  ninja: true,
  hunger: 4,
  // eat: function(isHungry) {
  //   if (isHungry > 5) {
  //     console.log('Get me some of dat pizza!');
  //   } else if (isHungry <= 5) {
  //     console.log('I\'m full Brah');
  //   }
  // }
};
console.log(turtle);



// This will not work; we have to assign/reassign a new method to the object property
// function turtle.eat() {
//   // do some code
// }

turtle.eat = function(isHungry) {
  if (isHungry > 5) {
    console.log('Get me some of dat pizza!');
  } else if (isHungry <= 5) {
    console.log('I\'m full Brah');
  }
}; // Declare your methods outside the object to keep them clear and concise

turtle.getColor = function() {
  return this.color;
};

turtle.randomHunger = function(min, max) {
  return Math.random() * (max - min) + min;
};  // e.g.  turtle.hunger = turtle.randomHunger(0, 50);




// Quick review of when to use semi-colons
// function beans() {
//   // Do some code
// }
//
// var beans = function() {
//   // Do some code
// };



// This is a basic create html and render to page example
// var sectionEl = document.getElementById('myList');
// sectionEl.textContent = 'Hello World!';
//
// var ulEl = document.createElement('ul');
// var liEl = document.createElement('li');
// liEl.textContent = 'I like grapes.';
//
// ulEl.appendChild(liEl);
// // target.appendChild(content)   (common syntax example for a method such as appendChild)
//
// sectionEl.appendChild(ulEl);


// This is a more advanced for loop inside for loop for nested lists
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var weeks = [1, 2, 3, 4];
var sectionEl = document.getElementById('myList');
var ulEl = document.createElement('ul');

for(month in months) {
  var liEl = document.createElement('li');
  liEl.textContent = months[month];
  var nestedUlEl = document.createElement('ul');

  for(week in weeks) {
    var nestedLiEl = document.createElement('li');
    nestedLiEl.textContent = weeks[week];
    nestedUlEl.appendChild(nestedLiEl);
  }

  liEl.appendChild(nestedUlEl);
  ulEl.appendChild(liEl);
}

sectionEl.appendChild(ulEl);
