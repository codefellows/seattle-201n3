#Survey Responses
##WHICH ASPECTS OF THE TRAINING DID YOU FIND THE MOST HELPFUL DURING THIS WEEK?

Code reviews with the instructor are always an excellent use of time IMO....

As before, the instructor was the most helpful thing this week. Plus, I'm getting a lot of good help and information from other students as they start to grasp concepts.

I think I appreciated the instructor's timeliness when it came to looking at our broken code. My partner and I tried to wait as long as we could before asking questions, but eventually had two moments where we were lost. As far as I know, we were given enough guidance without being given too much...so that we still had to make sure it worked ourselves.

I found the extra credit that focused on git and developer tools to be very useful.

Lab time, examples, and teachers. The explanation on git/github was also helpful. And coffee.

The extra clarification of git and github was really helpful.

Git review!

The most helpful part was building muscle memory for coding.

Code review. The same as last week. Being able to talk through code and why certain design choices were made as well as best practices has been the most helpful aspect of class by far. Once again Scott did an excellent job of explaining our process and best practices to follow.

Code review. I attribute my major breakthrough this week to seeing the code of a classmate. Seeing their code allowed me to think about the problem that we were working on in a completely different way. That understanding allowed several key concepts that we've been working on fall into place.

Reading the code helps a lot.

Using the lectures posted on youtube as a refresher for content I struggled with earlier in the week.

Watching you go through the different examples in atom and following along with you during class is the most helpful. Helps me retain the material better.

Having Scott demo code on the screen, then use verbal explanations and whiteboarding to augment the demo was great. Also, Scott is an amazing instructor. That he's willing to stop in the middle of a thought and take a question, even if that question requires him to backtrack to 5 minutes ago and recover information is priceless.

Scott's teaching is a bit more linear and not as tangential as some I've encountered. Also, the actual labwork is invaluable to entrenchment.

I found the lab assignments the most helpful - engaging with the material allows me to learn it much better.

I think having the extra instructional lecture and instructional lab was helpful. I appreciate that there was a lot of direction for the first week of class.

I found the opportunity to manipulate the DOM via textContent and className as a nice introduction to the heavier stuff that follows. It was nice to have that experience of feeling like I get it before we move on to tougher content.

I liked that our professor and TA's were very helpful.

The most helpful aspect of the training is having the TA(s) assistance in case I get stuck.

Scott keeps the pace and itinerary fairly manageable for the lectures so it's nice when the material we need to cover comes in bite sized pieces versus a firehose....

Having multiple opportunities to work with the terminal was helpful. Also, having TA's to help when you get stuck.

The TA's helped a lot.

lab work



##WHICH ASPECTS OF THE TRAINING DID YOU FIND THE LEAST HELPFUL DURING THIS WEEK?

Pair programming. I understand why we do it, I really do. I just hate it. I prefer to work on my own stuff...it's just frustrating having my code in the hands of someone else even though I'm dictating to them what to type. Maybe it's a control thing, but this class and my path at CF is so important to me that I feel better working on my own code. But I'm just weird. :-)

Some of the readings.

Didn't have much to do during cowork lab so that wasn't so useful this last week.

Paired programming. My partner was not prepared so the majority of our paired programming exercises were focused on just getting them current in their own work. I ended up having to do most of my work outside of class to just to stay current with what we were supposed to accomplish in class. I understand the value of paired programming and I did manage to gain some valuable insight in working with other people's code, but of all the things that we did this week, this was by far the most frustrating.

I can't say anything was really unhelpful, but I suppose having more explicit user stories for the assignment in these first few weeks would have been nice. I know of one classmate who thought he'd nailed the assignment because he made separate functions for each variable instead of putting them all into one. This was possibly a waste of time for him. I also questioned if I was doing the assignment correctly at one time, but I also recognize that this may be more like a work environment where outcomes or deliverables are intentionally or unintentionally vague.

We were left alone a fair amount, probably by design, when doing pair programming for the N4 assignment. Could have used more guidance especially since we didn't have the TAs around for the Thurs evening lab. 2+ days of spinning my wheels in relation to the overall class length is too long.

Alot of the first week just felt like a review of the prework. It was a nice little refresher but I didn't feel like I needed it.

I've discovered I have a general issue with the lecture format, partially because of the schedule. It's hard to listen and stay engaged, regardless of the live coding because of the hour (8:30pm is a giant wall for me, like Great Wall of China sized wall) . Sometimes, I don't see a point to live coding along with Scott since I can review the live code that he wrote later - it's also hard to type and listen at the same time while maintaining clean/organized code on my own screen. I understand the impetus for for live coding in the learning process, and I'm unsure of how to improve this process.

Reading without concrete examples. Lack of printouts explaining key concepts?

Not really a problem but it seems there was less resources available for linux and windows use.

I felt like we could have spent more time in the terminal.

The temp space isn't ideal so the changeover to the new spot will be welcome.

If I must write something here, I suppose that, for our first assignment/project, it "felt" like there was little direction. I was like, "wait, I think we're just supposed to read this document to figure out what to do." I think, in that moment, I was hoping for a read through of each step (even though parts of the assignment had been mentioned before). Maybe because it was our first one, I wanted to hear the instructor say, "okay, let's go over exactly what you need to do to make sure you know you're not missing any steps." I'm also okay with this though. At work, your boss isn't going to hold your hand through something like this, so I dont expect hand-holding here either.

I don't know what I would changed. I had difficulties in some areas, but right now I'm getting the help I need.

fast lectures without printout notes. It would be helpful if print out of the coding is given to students. For example step by step git hub instruction how to do add, commit push, pull request. Some of the codes teacher wrote on the screen.

##DO YOU HAVE ANY QUESTIONS, SUGGESTIONS, OR CONCERNS?

On the likert scale above, how is a 6 "cruising along and having fun"? That description seems more fitting of an 8. Just my two cents.

Anxious for the new space to be ready to move into.

Nothing at this time. To this point, my experience at Code Fellows has been wonderful. Oh, well, it would be nice if students got some kind of CF swag. Tshirts or something. I don't know about my fellow students, but this is my college...so it would be nice to have something sporting the CF logo.

Refactoring examples would be nice.

Individual's code should be evaluated before partners are assigned in paired programming. That way people with the same amounts of progress can work together and hopefully have complementary insights on their code.

I'm having hard time thinking about how to write the code.

How do you recommend we use our time in cowork lab? Should we attempt assignments during this time?

I know we're all adults and expected to have everything needed (programs wise) loaded and installed and configured before class. But it would be nice if during the opening class, while we're doing all of the introductory stuff, some time was given to making sure we all have the proper settings and configurations on our systems. I learned that there were a few packages in Atom that I didn't have (that weren't mentioned in the prework instructions) and occasionally found myself having to split my attention between the lecture and troubleshooting Atom because it wasn't showing me the expected things or reacting in an expected way.

Is it possible to get more analogy in the code examples? Especially for key concepts?
