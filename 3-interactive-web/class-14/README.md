# Class-14

# Reading Reminder
* [Chart.js and Canvas API Articles](https://canvas.instructure.com/courses/1015286/modules/items/9246725)

# Assignment for Class

Using ChartJS, add a bar chart to the app under the images on the page, where your list/table used to reside. You can get rid of the list/table now... but the process of making it gave you the command over your data that you'll need for making a chart, so it was not wasted energy or effort. In fact, you may want to comment it out and keep it around as a reference until you have the chart going.

Here's some resources and tips on getting Chart.js added to your vote tracker:
- The Chart.js docs, linked in resources below, which include a lot of helpful code examples
- Remember: the key to getting a chart going is the three pieces that Scott mentioned in class... check your notes for a reminder.
- Most important: get your data packed up properly
- Did you remember your `<canvas>` element?

As we've been doing all along, you should be working on different things in different branches in your repo, pushing them all to GitHub along the way, and merging them when things are the way you want. You've been doing that, right?

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246726)

## User Stories (MVP)


## Technical Requirements
 - Work completed on a branch, with great, regular commit history, and well written commit messages
 - Bar chart completed as part of the MVP (If you choose to try another chart after you finish the bar chart, do it on a separate branch.)
 - Updated README

## User Stories (Stretch)


## Helpful Resources
[Problem Domain](../assets/README.md)
[Raw Image Assets](../assets/imgs)
[Chart.JS Docs](http://www.chartjs.org/docs)
