# Class-13
[**Week 6 Feedback**]()
*You will receive an email from Code Fellows with a link to the survey. Once complete, please complete this assignment as well.*

# Reading Reminder
* ["Images" (HTML & CSS, Ch. 16)](https://canvas.instructure.com/courses/1015286/modules/items/9246722)

# Assignment for Class
Complete the initial setup and scaffolding for this project using your standard practices for creating a new project & repository.

**IN-CLASS WORKSHOP: Write User Stories for Assignment**

In this assignment, on page load, you will be displaying three random images to the DOM from your stock photo assets. Your user will select/click one image from the set of three. On click, the image will log an incremental vote, and then the page will load three new random images.

[Submit Your Work](https://canvas.instructure.com/courses/1015286/modules/items/9246723)

## User Stories (MVP)
**USER**

**DEVELOPER**

**MARKETER**

## Technical Requirements
 - New GitHub repo set up, and working on a feature branch
 - Project scaffolding complete, with proper file and folder structure
 - Images have been properly sized for project
 - Object constructor implemented and used properly for creating objects
 - Object literal implemented and used properly for managing the functionality of the app; proper use of methods and properties

## User Stories (Stretch)


## Helpful Resources
[Problem Domain](../assets/README.md)
[Raw Image Assets](../assets/imgs)
